#include <Arduino.h>
#include <WiFi.h>
#include <WiFiAP.h>
#include <WiFiClient.h>
#include <WiFiGeneric.h>
#include <WiFiMulti.h>
#include <WiFiScan.h>
#include <WiFiServer.h>
#include <WiFiSTA.h>
#include <WiFiType.h>
#include <WiFiUdp.h>
#include <ESPAsyncWebServer.h>
#include <Wire.h>

// WiFi设置
const char* ssid = "YourWiFiSSID";
const char* password = "YourWiFiPassword";

// I2C设置
const int I2C_MOTOR_ADDRESS = 0x40;  // 电机控制板的I2C地址
const int SDA_PIN = 21;  // I2C数据引脚
const int SCL_PIN = 22;  // I2C时钟引脚

// 电机控制命令
const byte MOTOR_STOP = 0x00;
const byte MOTOR_FORWARD = 0x01;
const byte MOTOR_REVERSE = 0x02;

// 创建AsyncWebServer对象
AsyncWebServer server(80);

// 当前电机状态
volatile int motorState = 0;  // 0:停止, 1:正转, 2:反转

// HTML页面
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body { font-family: Arial; text-align: center; margin: 0px auto; padding: 20px; }
        .button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;
                text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer; border-radius: 4px; }
        .button.stop { background-color: #f44336; }
    </style>
</head>
<body>
    <h2>刷鞋机控制面板</h2>
    <p><button class="button" onclick="controlMotor('/forward')">正转</button></p>
    <p><button class="button stop" onclick="controlMotor('/stop')">停止</button></p>
    <p><button class="button" onclick="controlMotor('/reverse')">反转</button></p>
    <script>
    function controlMotor(action) {
        fetch(action)
            .then(response => {
                if (!response.ok) throw Error(response.statusText);
                return response.text();
            })
            .then(data => console.log(data))
            .catch(error => console.error(error));
    }
    </script>
</body>
</html>
)rawliteral";

void sendMotorCommand(byte command) {
    Wire.beginTransmission(I2C_MOTOR_ADDRESS);
    Wire.write(command);
    Wire.endTransmission();
}

void setup() {
    // 初始化串口通信
    Serial.begin(115200);
    
    // 初始化I2C
    Wire.begin(SDA_PIN, SCL_PIN);
    
    // 连接WiFi
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.println("正在连接WiFi...");
    }
    Serial.println("WiFi连接成功");
    Serial.print("IP地址: ");
    Serial.println(WiFi.localIP());
    
    // 配置Web服务器路由
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send_P(200, "text/html", index_html);
    });
    
    server.on("/forward", HTTP_GET, [](AsyncWebServerRequest *request){
        motorState = 1;
        sendMotorCommand(MOTOR_FORWARD);
        request->send(200, "text/plain", "电机正转");
    });
    
    server.on("/stop", HTTP_GET, [](AsyncWebServerRequest *request){
        motorState = 0;
        sendMotorCommand(MOTOR_STOP);
        request->send(200, "text/plain", "电机停止");
    });
    
    server.on("/reverse", HTTP_GET, [](AsyncWebServerRequest *request){
        motorState = 2;
        sendMotorCommand(MOTOR_REVERSE);
        request->send(200, "text/plain", "电机反转");
    });
    
    // 启动服务器
    server.begin();
    
    // 初始化电机为停止状态
    sendMotorCommand(MOTOR_STOP);
    Serial.println("刷鞋机初始化完成");
}

void loop() {
    // 主循环中不需要额外的代码
    // 所有的控制都通过Web请求处理
    delay(100);
}
